var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "laravel_test"
});

var conPool = mysql.createPool({
  connectionLimit: 10, // default = 10
  host: "localhost",
  user: "root",
  password: "",
  database: "laravel_test"
});

var exports = (module.exports = {});

exports.connectdb = function() {
  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });
  return con;
};

exports.destroy = function() {
  con.end(function(err) {
    if (err) throw err;
    console.log("Ended!");
  });
};

exports.get_connection = function(){
  return con;
}
exports.get_connection_pool = function(){
  return conPool;
}
